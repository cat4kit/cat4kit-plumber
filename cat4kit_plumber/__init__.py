# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: EUPL-1.2

"""Cat4KIT Plumber

Within the Cat4KIT project, this repository is responsible for the installation, continuous maintenance, verifying pipeline running status of packages, version checker, and publication of pipeline roadmap charts.
"""

from __future__ import annotations

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "Mostafa Hadizadeh"
__copyright__ = "2023 Karlsruher Institut für Technologie"
__credits__ = [
    "Mostafa Hadizadeh",
]
__license__ = "EUPL-1.2"

__maintainer__ = "Mostafa Hadizadeh"
__email__ = "mostafa.hadizadeh@kit.edu"

__status__ = "Pre-Alpha"
