# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0

import json
import os

import requests

# Get the list of all milestones in the Cat4KIT group as an dictionary
milestones_dic = dict()
# Finding title, start_date, due_date, and id of each milestone in Cat4KIT group
url = "https://codebase.helmholtz.cloud/api/v4/groups/16928/milestones/"
headers = {"Private-Token": os.environ["Cat4KIT_ToKeN"]}
req_milestones = requests.get(url, headers=headers)
milestones_json = json.loads(req_milestones.text)

milestones_dic["title"] = [i.get("title") for i in milestones_json]
milestones_dic["start_date"] = [i.get("start_date") for i in milestones_json]
milestones_dic["due_date"] = [i.get("due_date") for i in milestones_json]
milestones_dic["id"] = [i.get("id") for i in milestones_json]


# Finding the issues completing percentages in each milestone
# According to the https://docs.gitlab.com/ee/api/group_milestones.html#get-all-issues-assigned-to-a-single-milestone
# Milestone API endpoint doesn’t return issues from any subgroups so we have to use the following approach

complete_percentage_arr = []
for j in [i.get("title") for i in milestones_json]:
    url = (
        "https://codebase.helmholtz.cloud/api/v4/groups/16928/issues?milestone="
        + str(j)
        + "&per_page=999"
    )
    headers = {"Private-Token": os.environ["Cat4KIT_ToKeN"]}
    req_issues = requests.get(url, headers=headers)
    issues_json = json.loads(req_issues.text)
    issues_array = [i.get("state") for i in issues_json]
    if len(issues_array) == 0:
        complete_percentage_arr.append(0.0)
        print("No issues in the milestone: " + str(j))
    else:
        complete_percentage_arr.append(
            round(
                len(list(filter(lambda x: x == "closed", issues_array)))
                / len(issues_array)
                * 100,
                1,
            )
        )
milestones_dic["complete_percentage"] = complete_percentage_arr

# writing the roadmap in the README.md file
filename = "README.md"
# Determin the milestones that is related to Cat4KIT development
Cat4KIT_development = [
    "TDS2STAC V2",
    "Cat4KIT-UI V1",
    "DS2STAC V2",
    "Cat4KIT-UI V2",
    "Thumbnails",
    "Int. and Ext. harvester",
    "API Authentification",
    "Pipeline",
]
# Determin the milestones that is related to Cat4KIT docker development
Cat4KIT_docker = [
    "Dockerizing",
]
# Determin the milestones that is related to Cat4KIT research
Research = [
    "Comparison research",
    "Testify datasets",
]
# Determin the milestones that is related to Cat4KIT documentations
Documentations = [
    "DS2STAC doc",
    "Cat4KIT doc",
    "Cat4KIT article",
]
# Define constant variables
Cat4KIT_development_ = "# Cat4KIT pipeline roadmap \n \n```mermaid \n%%{init: { 'theme':'forest'}}%%\ngantt \n\
dateFormat YYYY-MM-DD \n\
title Cat4KIT development roadmap\n\
section Cat4KIT dev\n\
TDS2STAC V1(100%):done,tds2stac1,2022-10-01,2022-11-01\n\
INTAKE2STAC V1(100%):done,intake2stac,2022-11-01,2022-11-15\n\
STA2STAC V1(100%):done,sta2stac,2022-11-16,2022-11-30\n\
DS2STAC-UI V1(100%):done,ds2stac-ui1,2022-12-01,2022-12-10\n"
Cat4KIT_docker_ = "section Cat4KIT-docker\n\
+pgSTAC(100%):done,pgstacdocekr,2022-12-11,2022-12-25\n\
+STAC-FastAPI(100%):done,stacapdocker,2022-12-11,2022-12-25\n\
+STAC-Browser(100%):done,stacbrowserdocker,2022-12-11,2022-12-25\n\
+DS2STAC(100%):done,ds2stacdocker,2022-12-11,2022-12-25\n"
Research_ = "section Research\n"

Documentations_ = "section Docs and Publications\n\
DS2STAC Docs(100%):done,tds2stacdoc,2022-10-01,2022-11-30\n\
Datahub(100%):done,datahub,2023-01-19,2023-01-20\n\
E-Sceince-Tage23(100%):done,esciencetage23,2023-03-01,2023-03-03\n\
EGU(100%):done,egu,2023-04-23,2023-04-28\n\
DSS8(100%):done,dss8,2023-06-08,2023-06-09\n"
About_Cat4KIT_Title = ": A cross-institutional data catalog framework for the FAIRification of environmental research data"
labels = [
    "[![CI](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber/-/pipelines?page=1&scope=all&ref=main)",
    "[![Docs](https://readthedocs.org/projects/cat4kit/badge/?version=latest)](https://cat4kit.readthedocs.io/en/latest/)",
    "[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)",
    "[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)",
]
About_Cat4KIT = "The objective of the Cat4KIT project is to create a cross-institutional catalog and research data management system that will enhance the Findability, Accessibility, Interoperability, and Reusability (FAIR) principles of environmental research data. The framework comprises four distinct modules, each assigned with certain tasks.\n\n (1) Facilitating the availability of data on storage systems via clearly defined and standardized interfaces. \n\n (2) The process of harvesting and creating (meta)data into uniform and standardized representations. \n\n (3) Facilitating the public accessibility to (meta)data by the utilization of well defined and standardized catalog services and interfaces, so ensuring consistency and uniformity.\n\n (4) Allowing users to efficiently search, apply filters, and navigate through data obtained from decentralized research data infrastructures.\n \n\n\nEvery module is created and executed in an inter-institutional cooperation consisting of scientists, software developers, and possible end-users. This methodology guarantees the versatility of our framework, allowing it to be applied to many types of research data, including multi-dimensional climate model outputs and high-frequency in-situ measurements."

pipeline_stat = "## Cat4KIT Pipeline Status \n \n```mermaid \n\nmindmap\n\
    root((Cat4KIT Docker)) \n\
        DS2STAC\n\
            UNICODE(TDS2STAC ✅) \n\
            UNICODE(INTAKE2STAC ✅) \n\
            UNICODE(STA2STAC ✅) \n\
            UNICODE(INSUPDEL4STAC ✅) \n\
        Cat4KIT UI\n\
            Backend\n\
                UNICODE(Scheduler ✅) \n\
                UNICODE(Logger ✅) \n\
                UNICODE(Once Task ✅) \n\
                UNICODE(Bridge ✅) \n\
            Frontend\n\
        STAC\n\
            UNICODE(STAC FastAPI ✅) \n\
            UNICODE(pgSTAC ✅) \n\
            UNICODE(STAC Browser ✅)"
pipeline_stat_names = [
    "Bridge",
    "Logger",
    "Once-Task",
    "Scheduler",
    "INTAKE2STAC",
    "INSUPDEL4STAC",
    "STA2STAC",
    "TDS2STAC",
]
pipeline_stat_addresses = [
    "https://codebase.helmholtz.cloud/api/v4/projects/8944/jobs/1130168",
    "https://codebase.helmholtz.cloud/api/v4/projects/8943/jobs/1130175",
    "https://codebase.helmholtz.cloud/api/v4/projects/8870/jobs/1136397",
    "https://codebase.helmholtz.cloud/api/v4/projects/8942/jobs/1135242",
    "https://codebase.helmholtz.cloud/api/v4/projects/6970/jobs/923594",
    "https://codebase.helmholtz.cloud/api/v4/projects/8128/jobs/1111534",
]
license = "<!--\
SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie\
\
SPDX-License-Identifier: CC-BY-4.0\
-->\
"

technical_infos = "\
# Technical information\n\
\n\n\
This project employed various technologies, which are enumerated as follows. In addition, the documentation links and their corresponding unit test results are provided below.\n\
\n\n\
\
| Name    | Technologie | Styles | Docs | Tests |\n\
|:----------:|:-------------:|:-------------:|:-------------:|:-------------:|\n\
|TDS2STAC|![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)|[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/) <br /> [![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/) [![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)|[![Docs](https://readthedocs.org/projects/tds2stac/badge/?version=latest)](https://tds2stac.readthedocs.io/en/latest/)|[![CI](https://codebase.helmholtz.cloud/cat4kit/ds2stac/tds2stac/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/ds2stac/tds2stac/-/pipelines?page=1&scope=all&ref=main) [![Code coverage](https://codebase.helmholtz.cloud/cat4kit/ds2stac/tds2stac/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/ds2stac/tds2stac/-/graphs/main/charts)|\n\
|STA2STAC|![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)|[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/) <br /> [![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/) [![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)|[![Docs](https://readthedocs.org/projects/sta2stac/badge/?version=latest)](https://sta2stac.readthedocs.io/en/latest/)|[![CI](https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac/-/pipelines?page=1&scope=all&ref=main) [![Code coverage](https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac/-/graphs/main/charts)|\n\
|INTAKE2STAC|![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)|[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/) <br /> [![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/) [![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/) |[![Docs](https://readthedocs.org/projects/intake2stac/badge/?version=latest)](https://intake2stac.readthedocs.io/en/latest/)|[![CI](https://codebase.helmholtz.cloud/cat4kit/ds2stac/intake2stac/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/ds2stac/intake2stac/-/pipelines?page=1&scope=all&ref=main) [![Code coverage](https://codebase.helmholtz.cloud/cat4kit/ds2stac/intake2stac/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/ds2stac/intake2stac/-/graphs/main/charts)|\n\
|INSUPDEL4STAC|![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)|[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/) <br /> [![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/) [![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/) |[![Docs](https://readthedocs.org/projects/insupdel4stac/badge/?version=latest)](https://insupdel4stac.readthedocs.io/en/latest/)|[![CI](https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/-/pipelines?page=1&scope=all&ref=main) [![Code coverage](https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/ds2stac/insupdel4stac/-/graphs/main/charts)|\n\
|Cat4KIT UI Backend Once Task|![DjangoREST](https://img.shields.io/badge/DJANGO-REST-ff1709?style=for-the-badge&logo=django&logoColor=white&color=ff1709&labelColor=gray) ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white) <br /> ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)|[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/) <br />  [![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/) [![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/) |-|[![CI](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/-/pipelines?page=1&scope=all&ref=main) [![Code coverage](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/-/graphs/main/charts)|\n\
|Cat4KIT UI Backend Scheduler|![DjangoREST](https://img.shields.io/badge/DJANGO-REST-ff1709?style=for-the-badge&logo=django&logoColor=white&color=ff1709&labelColor=gray) ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white) <br /> ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)|[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/) <br /> [![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/) [![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/) |-|[![CI](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/-/pipelines?page=1&scope=all&ref=main) [![Code coverage](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/-/graphs/main/charts)|\n\
|Cat4KIT UI Backend Bridge|![DjangoREST](https://img.shields.io/badge/DJANGO-REST-ff1709?style=for-the-badge&logo=django&logoColor=white&color=ff1709&labelColor=gray) ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white) <br /> ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)|[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/) <br />  [![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/) [![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)|-|[![CI](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/-/pipelines?page=1&scope=all&ref=main) [![Code coverage](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-bridge/-/graphs/main/charts)|\n\
|Cat4KIT UI Front-end|![Vue.js](https://img.shields.io/badge/vuejs-%2335495e.svg?style=for-the-badge&logo=vuedotjs&logoColor=%234FC08D) ![Vite](https://img.shields.io/badge/vite-%23646CFF.svg?style=for-the-badge&logo=vite&logoColor=white) <br /> ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E) | ![TailwindCSS](https://img.shields.io/badge/tailwindcss-%2338B2AC.svg?style=for-the-badge&logo=tailwind-css&logoColor=white)|-|-|\n\
|STAC Browser|![Vue.js](https://img.shields.io/badge/vuejs-%2335495e.svg?style=for-the-badge&logo=vuedotjs&logoColor=%234FC08D) ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)|-|-|-|\n\
|STAC API|![FastAPI](https://img.shields.io/badge/FastAPI-005571?style=for-the-badge&logo=fastapi) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)|-|[![Documentation](https://img.shields.io/github/actions/workflow/status/stac-utils/stac-fastapi-pgstac/pages.yml?label=Docs&style=for-the-badge)](https://stac-utils.github.io/stac-fastapi-pgstac/)|[![CI](https://img.shields.io/github/actions/workflow/status/stac-utils/stac-fastapi-pgstac/cicd.yaml?style=for-the-badge)](https://github.com/stac-utils/stac-fastapi-pgstac/actions/workflows/cicd.yaml)|\
|pgSTAC|![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)|-|[![Docs](https://readthedocs.org/projects/tds2stac/badge/?version=latest)](https://stac-utils.github.io/pgstac/)|[![CI](https://github.com/stac-utils/pgstac/workflows/CI/badge.svg)](https://github.com/stac-utils/pgstac/actions?query=workflow%3ACI)|\n\
\n\n\
"
with open(filename, "w") as file:
    file.write(license + "\n")
    file.write(
        "#### "
        + "![Cat4KIT](https://codebase.helmholtz.cloud/uploads/-/system/group/avatar/16928/Bildschirmfoto_2024-02-09_um_08.13.52.png?width=64)"
        + About_Cat4KIT_Title
        + "\n"
    )
    file.write("____ \n")
    for i in labels:
        file.write(i + "\n")
    file.write("\n\n")
    file.write(About_Cat4KIT + "\n")
    file.write("\n\n")
    # file.write(technical_infos + "\n")
    # file.write("\n\n")
    # file.write(pipeline_stat + "\n")
    # file.write("```\n")
    # file.write("\n\n")
    file.write(Cat4KIT_development_)  # type: ignore
    for i in range(len(milestones_dic.get("title"))):  # type: ignore
        if (
            milestones_dic["title"][i] in Cat4KIT_development  # type: ignore
            and milestones_dic["complete_percentage"][i] != 100  # type: ignore
        ):  # type: ignore
            file.write(
                milestones_dic["title"][i]  # type: ignore
                + "("
                + str(milestones_dic["complete_percentage"][i])  # type: ignore
                + "%)"
                + ":active, "
                + str(milestones_dic["id"][i])  # type: ignore
                + ", "
                + str(milestones_dic["start_date"][i])  # type: ignore
                + ", "
                + str(milestones_dic["due_date"][i])  # type: ignore
                + "\n"
            )
        elif (
            milestones_dic["title"][i] in Cat4KIT_development  # type: ignore
            and milestones_dic["complete_percentage"][i] == 100  # type: ignore
        ):
            file.write(
                milestones_dic["title"][i]  # type: ignore
                + "("
                + str(milestones_dic["complete_percentage"][i])  # type: ignore
                + "%)"
                + ":done, "
                + str(milestones_dic["id"][i])  # type: ignore
                + ", "
                + str(milestones_dic["start_date"][i])  # type: ignore
                + ", "
                + str(milestones_dic["due_date"][i])  # type: ignore
                + "\n"
            )
    file.write(Cat4KIT_docker_)
    for i in range(len(milestones_dic.get("title"))):  # type: ignore
        if (
            milestones_dic["title"][i] in Cat4KIT_docker  # type: ignore
            and milestones_dic["complete_percentage"][i] != 100  # type: ignore
        ):
            file.write(
                milestones_dic["title"][i]  # type: ignore
                + "("
                + str(milestones_dic["complete_percentage"][i])  # type: ignore
                + "%)"
                + ":active, "
                + str(milestones_dic["id"][i])  # type: ignore
                + ", "
                + str(milestones_dic["start_date"][i])  # type: ignore
                + ", "
                + str(milestones_dic["due_date"][i])  # type: ignore
                + "\n"
            )
        elif (
            milestones_dic["title"][i] in Cat4KIT_docker  # type: ignore
            and milestones_dic["complete_percentage"][i] == 100  # type: ignore
        ):
            file.write(  # type: ignore
                milestones_dic["title"][i]  # type: ignore
                + "("
                + str(milestones_dic["complete_percentage"][i])  # type: ignore
                + "%)"
                + ":done, "
                + str(milestones_dic["id"][i])  # type: ignore
                + ", "
                + str(milestones_dic["start_date"][i])  # type: ignore
                + ", "
                + str(milestones_dic["due_date"][i])  # type: ignore
                + "\n"
            )
    file.write(Research_)
    for i in range(len(milestones_dic.get("title"))):  # type: ignore
        if (
            milestones_dic["title"][i] in Research  # type: ignore
            and milestones_dic["complete_percentage"][i] != 100  # type: ignore
        ):
            file.write(
                milestones_dic["title"][i]  # type: ignore
                + "("
                + str(milestones_dic["complete_percentage"][i])  # type: ignore
                + "%)"
                + ":active, "
                + str(milestones_dic["id"][i])  # type: ignore
                + ", "
                + str(milestones_dic["start_date"][i])  # type: ignore
                + ", "
                + str(milestones_dic["due_date"][i])  # type: ignore
                + "\n"
            )
        elif (
            milestones_dic["title"][i] in Research  # type: ignore
            and milestones_dic["complete_percentage"][i] == 100  # type: ignore
        ):
            file.write(
                milestones_dic["title"][i]  # type: ignore
                + "("
                + str(milestones_dic["complete_percentage"][i])  # type: ignore
                + "%)"
                + ":done, "
                + str(milestones_dic["id"][i])  # type: ignore
                + ", "
                + str(milestones_dic["start_date"][i])  # type: ignore
                + ", "
                + str(milestones_dic["due_date"][i])  # type: ignore
                + "\n"
            )
    file.write(Documentations_)
    for i in range(len(milestones_dic.get("title"))):  # type: ignore
        if (
            milestones_dic["title"][i] in Documentations  # type: ignore
            and milestones_dic["complete_percentage"][i] != 100  # type: ignore
        ):
            file.write(
                milestones_dic["title"][i]  # type: ignore
                + "("
                + str(milestones_dic["complete_percentage"][i])  # type: ignore
                + "%)"
                + ":active, "
                + str(milestones_dic["id"][i])  # type: ignore
                + ", "
                + str(milestones_dic["start_date"][i])  # type: ignore
                + ", "
                + str(milestones_dic["due_date"][i])  # type: ignore
                + "\n"
            )
        elif (
            milestones_dic["title"][i] in Documentations  # type: ignore
            and milestones_dic["complete_percentage"][i] == 100  # type: ignore
        ):
            file.write(
                milestones_dic["title"][i]  # type: ignore
                + "("
                + str(milestones_dic["complete_percentage"][i])  # type: ignore
                + "%)"
                + ":done, "
                + str(milestones_dic["id"][i])  # type: ignore
                + ", "
                + str(milestones_dic["start_date"][i])  # type: ignore
                + ", "
                + str(milestones_dic["due_date"][i])  # type: ignore
                + "\n"
            )
    file.write("```\n")
    file.write("\n")
    file.write("# Cat4KIT schematic diagram (Will be updated) \n")
    file.write(
        "[![Cat4KIT](https://codebase.helmholtz.cloud/cat4kit/gitlab-profile/-/raw/main/Cat4KIT_schematic.png)](https://www.mindmeister.com/map/2809466926?t=36rTXj1fJQ)"
    )
