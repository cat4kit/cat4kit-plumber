# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: EUPL-1.2

"""Test file for imports."""


def test_package_import():
    """Test the import of the main package."""
    import cat4kit_plumber  # noqa: F401
