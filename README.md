<!--
SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

SPDX-License-Identifier: CC-BY-4.0
-->

# Cat4KIT Plumber

[![CI](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber/-/graphs/main/charts)
<!-- TODO: uncomment the following line when the package is registered at https://readthedocs.org -->
<!-- [![Docs](https://readthedocs.org/projects/cat4kit-plumber/badge/?version=latest)](https://cat4kit-plumber.readthedocs.io/en/latest/) -->
[![Latest Release](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber/-/badges/release.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber)
<!-- TODO: uncomment the following line when the package is published at https://pypi.org -->
<!-- [![PyPI version](https://img.shields.io/pypi/v/cat4kit-plumber.svg)](https://pypi.python.org/pypi/cat4kit-plumber/) -->
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
<!-- TODO: uncomment the following line when the package is registered at https://api.reuse.software -->
<!-- [![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/cat4kit/cat4kit-plumber)](https://api.reuse.software/info/codebase.helmholtz.cloud/cat4kit/cat4kit-plumber) -->


Within the Cat4KIT project, this repository is responsible for the installation, continuous maintenance, verifying pipeline running status of packages, version checker, and publication of pipeline roadmap charts.

All TOKENS are stored as protected variables within the CI.

Creating a Roadmap pipeline as a Gantt chart within a README.md file for the first page of the [Cat4KIT group](https://codebase.helmholtz.cloud/cat4kit) is the objective of this repository. The README.md file is then pushed to the [Cat4KIT repository](https://codebase.helmholtz.cloud/cat4kit/gitlab-profile).

This repository is configured to be executed daily as a CI.
For additional information, please review the source code.

It should be noted that we could use the [roadmap](https://docs.gitlab.com/ee/user/group/roadmap) feature instead, but because it is a paid add-on, we must stick with this for now.

## Installation

To execute this code for other groups, simply create milestones as time-bound assignments and add issues to them.

The following action is to simply deactivate CI notification for this repository. Because when there is no progress in the project, CI will notify you that the build has failed.

To use this in a development setup, clone the [source code][source code] from
gitlab, start the development server and make your changes::

```bash
git clone https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber
cd cat4kit-plumber
python -m venv venv
source venv/bin/activate
make dev-install
pip install -r requirements.txt
python cat4kit_plumber/roadmap_creator.py
```

More detailed installation instructions my be found in the [docs][docs].


[source code]: https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber
[docs]: https://cat4kit-plumber.readthedocs.io/en/latest/installation.html

## Technical note

This package has been generated from the template
https://codebase.helmholtz.cloud/hcdc/software-templates/python-package-template.git.

See the template repository for instructions on how to update the skeleton for
this package.


## License information

Copyright © 2023 Karlsruher Institut für Technologie



Code files in this repository are licensed under the
EUPL-1.2, if not stated otherwise
in the file.

Documentation files in this repository are licensed under CC-BY-4.0, if not stated otherwise in the file.

Supplementary and configuration files in this repository are licensed
under CC0-1.0, if not stated otherwise
in the file.

Please check the header of the individual files for more detailed
information.



### License management

License management is handled with [``reuse``](https://reuse.readthedocs.io/).
If you have any questions on this, please have a look into the
[contributing guide][contributing] or contact the maintainers of
`cat4kit-plumber`.

[contributing]: https://cat4kit-plumber.readthedocs.io/en/latest/contributing.html
