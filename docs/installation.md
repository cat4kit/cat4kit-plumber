<!--
SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

SPDX-License-Identifier: CC-BY-4.0
-->

(installation)=
# Installation

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of cat4kit-plumber!
```

To install the _cat4kit-plumber_ package, we
recommend that you install it from PyPi via

```bash
pip install cat4kit-plumber
```

Or install it directly from [the source code repository on
Gitlab][source code repository]
via:

```bash
pip install git+https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber.git
```

The latter should however only be done if you want to access the
development versions.

[source code repository]: https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber

(install-develop)=
## Installation for development

Please head over to our
`contributing guide <contributing>`{.interpreted-text role="ref"} for
installation instruction for development.
