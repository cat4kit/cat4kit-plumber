# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: EUPL-1.2

"""pytest configuration script for cat4kit-plumber."""

import pytest  # noqa: F401
